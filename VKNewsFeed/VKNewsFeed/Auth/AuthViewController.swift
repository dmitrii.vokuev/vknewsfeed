//
//  AuthViewController.swift
//  VKNewsFeed
//
//  Created by dmitrii on 18.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    private var authService: AuthService!
    override func viewDidLoad() {
        super.viewDidLoad()
     //   authService = AuthService()
        authService = AppDelegate.shared().authService
        
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        authService.wakeUpSession()
    }
    
}
