//
//  UserResponse.swift
//  VKNewsFeed
//
//  Created by dmitrii on 31.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation

struct UserResponseWrapped: Decodable {
    let response: [UserResponse]
}

struct UserResponse: Decodable {
    let photo100: String?
}
