//
//  API.swift
//  VKNewsFeed
//
//  Created by dmitrii on 20.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation

struct API {
    static let scheme = "https"
    static let host = "api.vk.com"
    static let version = "5.101"
    static let newsFeed = "/method/newsfeed.get"
    static let permission = ["messages","offline","friends","wall","photos","audio","video","docs"]
    static let user = "/method/users.get"
    
}
