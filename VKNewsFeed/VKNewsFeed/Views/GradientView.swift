//
//  GradientView.swift
//  VKNewsFeed
//
//  Created by dmitrii on 04.09.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    
    private let gradientlayer = CAGradientLayer()
    
    @IBInspectable private var startColor: UIColor? {
        didSet {
            setupGradientColors()
        }
    }
    @IBInspectable private var endColor: UIColor? {
        didSet {
            setupGradientColors()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientlayer.frame = bounds            
    }
    
    private func setupGradient() {
        self.layer.addSublayer(gradientlayer)
        setupGradientColors()
        gradientlayer.startPoint = CGPoint(x: 0.5, y: 0)
        gradientlayer.endPoint = CGPoint(x: 0.5, y: 1)

    }
    
    private func setupGradientColors() {
        if let statrColor = startColor, let endColor = endColor {
            gradientlayer.colors = [statrColor.cgColor, endColor.cgColor]
        }
    }
}
