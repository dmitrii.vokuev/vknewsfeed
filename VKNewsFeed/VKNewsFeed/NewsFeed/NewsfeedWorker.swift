//
//  NewsfeedWorker.swift
//  VKNewsFeed
//
//  Created by dmitrii on 21.08.19.
//  Copyright (c) 2019 dmitrii. All rights reserved.
//

import UIKit

class NewsfeedService {
    
    var authService: AuthService
    var networking: Networking
    var fetcher: DataFetcher
    
    private var revealPostIds = [Int]()
    private var feedResponse: FeedResponse?
    private var newFromInProcces: String?

    
    init() {
        self.authService = AppDelegate.shared().authService
        self.networking = NetworkService(authService: authService)
        self.fetcher = NetworkingDataFetcher(networking: networking)
    }
    
    func getUser(complition: @escaping (UserResponse?) -> Void) {
        fetcher.getUser { (userResponse) in
            complition(userResponse)
        }
    }
    
    func getFeed(completion: @escaping ([Int], FeedResponse) -> Void) {
        fetcher.getFeed(nextBatchFrom: nil) { [weak self] (feed) in
            self?.feedResponse = feed
            guard let feedResponse = self?.feedResponse else { return }
            completion(self!.revealPostIds, feedResponse)
        }
    }
    
    func revealPostIds(forPostId postId: Int, completion: @escaping ([Int], FeedResponse) -> Void) {
        revealPostIds.append(postId)
        guard let feedResponse = self.feedResponse else { return }
        completion(revealPostIds, feedResponse)
    }
    
    func removePostIdsFromRevealList(forPostId postId: Int, completion: @escaping ([Int], FeedResponse) -> Void) {
     let filtredPostID = revealPostIds.filter { $0 != postId }
        guard let feedResponse = self.feedResponse else { return }
        completion(filtredPostID, feedResponse)
    }
    
    func getNextBatch(completion: @escaping ([Int], FeedResponse) -> Void) {
        newFromInProcces = feedResponse?.nextFrom
        fetcher.getFeed(nextBatchFrom: newFromInProcces) { [weak self] (feed) in
            guard let feed = feed else { return }
            guard self?.feedResponse?.nextFrom != feed.nextFrom else { return }
            if self?.feedResponse == nil {
                self?.feedResponse = feed
            } else {
                self?.feedResponse?.items.append(contentsOf: feed.items)
                
                var profiles = feed.profiles
                if let oldProfiles = self?.feedResponse?.profiles {
                    let oldProfilesFiltered = oldProfiles.filter({ (oldProfile) -> Bool in
                        !feed.profiles.contains(where: { $0.id == oldProfile.id })
                    })
                    profiles.append(contentsOf: oldProfilesFiltered)
                }
                self?.feedResponse?.profiles = profiles
                
                var groups = feed.groups
                if let oldGroups = self?.feedResponse?.groups {
                    let oldGroupsFiltered = oldGroups.filter({ (oldGroups) -> Bool in
                        !feed.groups.contains(where: { $0.id == oldGroups.id  })
                    })
                    groups.append(contentsOf: oldGroupsFiltered)
                }
                self?.feedResponse?.groups = groups
                self?.feedResponse?.nextFrom = feed.nextFrom
            }
            
            guard let feedResponse = self?.feedResponse else { return }
            
            completion(self!.revealPostIds, feedResponse)
        }
    }
}
