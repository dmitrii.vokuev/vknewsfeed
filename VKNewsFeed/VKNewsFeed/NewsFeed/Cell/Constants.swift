//
//  Constants.swift
//  VKNewsFeed
//
//  Created by dmitrii on 25.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let cardInsets = UIEdgeInsets(top: 0, left: 8, bottom: 12, right: 8)
    static let topViewHeight: CGFloat = 46
    static let postLabelInsets = UIEdgeInsets(top: 8 + Constants.topViewHeight + 10, left: 9, bottom: 8, right: 8)
    static let postLabelFont = UIFont.systemFont(ofSize: 15)
    static let bottomViewHeight: CGFloat = 45
    
    // bottom view
    static let bottomViewViewHeight: CGFloat = 45
    static let bottomViewViewWidth: CGFloat = 80

    // views on bottom view
    static let bottomViewViewsIconSize: CGFloat = 24
    
    static let minifiedPostLimitLines: CGFloat = 8
    static let minifiedPostLine: CGFloat = 6
    
    static let moreTextButtonInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    static let moreOrLessTextButtonSize = CGSize(width: 170, height: 30)
}
